// imports.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <windows.h>
#include <tlhelp32.h>
#include <iostream>
#include <tchar.h>
#include <cassert>

using namespace std;

DWORD PID;
HANDLE process;
 
extern "C" __declspec(dllexport) inline void __cdecl setPID() {
	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	if (Process32First(snapshot, &entry) == TRUE) {
		while (Process32Next(snapshot, &entry) == TRUE) {
			wstring binPath = entry.szExeFile;
			if (binPath.find(L"WoW.exe") != wstring::npos) { //L"WoW.exe" //processName
				printf("game pid is %d\n", entry.th32ProcessID);
				PID = entry.th32ProcessID;
				break;
			}
		}
	}
}

extern "C" __declspec(dllexport) inline DWORD __cdecl getPID() {
	return PID;
}

extern "C" __declspec(dllexport) inline void __cdecl openProcess() {//HANDLE
	//open process using window's PID.
	process =
		OpenProcess(
		PROCESS_VM_OPERATION |
		PROCESS_VM_READ |
		PROCESS_VM_WRITE,
		FALSE,
		PID
	);
}

//extern "C" 
//extern "C" __declspec(dllexport) inline DWORD __cdecl readMemoryDword(DWORD adr) {
//	DWORD val;
//	ReadProcessMemory(process, (LPVOID)adr, &val, sizeof(val), NULL);
//	return val;
//}
extern "C" __declspec(dllexport) float __cdecl readMemoryFloat() { //__cdecl
	float kek;
	ReadProcessMemory(process, (LPVOID)0x11392678, &kek, sizeof(kek), NULL);
	return kek;
}