Red/System []

#define imports "G:\Red_Memory_Manipulation\imports\imports.dll"
#define calling cdecl
#import [
	imports calling [
		set-pid: "setPID" []
		get-pid: "getPID" [
			return: [integer!]
		]
		open-process: "openProcess" []
		read-memory: "readMemoryFloat" [
			;address [integer!]
			return: [float!]
		]
	]
]

set-pid
print get-pid
print read-memory